import datetime
from random import choice, randint

import pytz
import factory
from factory.fuzzy import FuzzyDateTime, FuzzyChoice
from taggit.models import Tag

from .models import Article, STATUS_CHOICES


class TagFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Tag
        django_get_or_create = ('name',)

    name = factory.Faker("word")


class ArticleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Article

    url = factory.Faker("url")
    lang = "EN"

    title = factory.Faker("sentence", nb_words=4)
    website = factory.Sequence(lambda n: "Website {}".format(n))
    extracts = factory.Faker("text")

    created_at = FuzzyDateTime(
        datetime.datetime(2014, 1, 1, tzinfo=pytz.UTC))
    updated_at = FuzzyDateTime(
        datetime.datetime(2014, 1, 1, tzinfo=pytz.UTC))
    published_at = FuzzyDateTime(
        datetime.datetime(2014, 1, 1, tzinfo=pytz.UTC))

    status = FuzzyChoice([s[0] for s in STATUS_CHOICES])
    archive = choice([True, False])
    quote = choice([True, False])
    speak = choice([True, False])

    @factory.post_generation
    def tags(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for tag in extracted:
                self.tags.add(tag)
