from django.test import TestCase

from rp import utils


class UtilsTests(TestCase):
    def test_cleanup_url(self):
        self.assertEqual(
            "http://example.com/",
            utils.cleanup_url(
                "http://example.com/?utm_source=dlvr.it&utm_medium=twitter&utm_campaign=social"
            ),
        )
        # Contrary to what's stated in the function docs, the following example fails with a UnicodeDecodeError
        # self.assertEqual(
        #     "http://\xc3\xbc.com/",
        #     utils.cleanup_url("http://ü.com/??"),
        # )
