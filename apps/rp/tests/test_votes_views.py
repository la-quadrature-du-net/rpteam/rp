from django.urls import reverse
from django.test import TestCase
from django.contrib.auth.models import Permission
from rest_framework.test import APIClient
from rest_framework.test import APIRequestFactory, force_authenticate

from userprofile.factories import ProfileFactory
from rp.factories import ArticleFactory
from rp.models import Article
from rp.api.views import ArticleViewSet


class VoteViewTestCase(TestCase):
    def setUp(self):
        self.article = ArticleFactory(status="NEW")
        self.article_draft = ArticleFactory(status="DRAFT")

        self.profile = ProfileFactory()
        self.user = self.profile.user

        p = Permission.objects.get(codename="can_vote")
        self.user.user_permissions.add(p)
        p = Permission.objects.get(codename="can_change_status")
        self.user.user_permissions.add(p)
        self.user.save()

        self.client = APIClient()
        self.client.force_authenticate(user=self.user)

    def test_votes(self):
        url_upvote = reverse("api:article-upvote", kwargs={
            "pk": self.article.id
        })

        url_downvote = reverse("api:article-downvote", kwargs={
            "pk": self.article.id
        })

        response = self.client.post(url_upvote)
        self.assertEqual(response.status_code, 200)
        article_db = Article.objects.get(id=self.article.id)
        assert article_db.score == 1

        response = self.client.post(url_upvote)
        self.assertEqual(response.status_code, 200)
        article_db.refresh_from_db()
        assert article_db.score == 2

        response = self.client.post(url_downvote)
        self.assertEqual(response.status_code, 200)
        article_db = Article.objects.get(id=self.article.id)
        assert article_db.score == 1

    def test_publish(self):
        url_publish = reverse("api:article-publish", kwargs={
            "pk": self.article_draft.id
        })

        response = self.client.post(url_publish)
        self.assertEqual(response.status_code, 200)
        article_db = Article.objects.get(id=self.article_draft.id)
        assert article_db.status == "PUBLISHED"

    def test_reject(self):
        url_reject = reverse("api:article-reject", kwargs={
            "pk": self.article.id
        })

        factory = APIRequestFactory()
        request = factory.post(url_reject)
        force_authenticate(request, user=self.user)
        view = ArticleViewSet.as_view({"post": "reject"})
        response = view(request, pk=self.article.id)
        self.assertEqual(response.status_code, 200)

        article_db = Article.objects.get(id=self.article.id)
        assert article_db.status == "REJECTED"

    def test_votes_error_if_publish(self):
        article = ArticleFactory(status="PUBLISHED")
        url_upvote = reverse("api:article-upvote", kwargs={
            "pk": article.id
        })

        response = self.client.post(url_upvote)
        assert response.status_code == 400
