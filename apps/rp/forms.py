from django import forms
from django.contrib.auth.models import Group

class SignupForm(forms.Form):
    def signup(self, request, user):
        group = Group.objects.get(name='padawan')
        user.groups.add(group)
        user.save()
