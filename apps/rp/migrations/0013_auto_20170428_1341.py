# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-04-28 13:41
from __future__ import unicode_literals

from django.db import migrations, models
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('rp', '0012_auto_20170427_1439'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='lang',
            field=models.CharField(choices=[('FR', 'French'), ('EN', 'English'), ('NA', 'Other')], default='NA', max_length=50, verbose_name='Language'),
        ),
        migrations.AlterField(
            model_name='article',
            name='tags',
            field=taggit.managers.TaggableManager(blank=True, help_text='A comma-separated list of tags.', through='taggit.TaggedItem', to='taggit.Tag', verbose_name='Tags'),
        ),
    ]
