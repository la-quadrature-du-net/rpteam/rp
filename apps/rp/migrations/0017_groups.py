from django.db import migrations
from django.contrib.auth.models import Group

groups = ['Jedi', 'Padawan', 'Droid']

def add_groups(apps, schema_editor):
    for i in groups:
        group = Group(name=i)
        group.save()

def remove_groups(apps, schema_editor):
    for i in groups:
        Group.objects.filter(name=i).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('rp', '0016_auto_20170617_0929'),
    ]

    operations = [
        migrations.RunPython(add_groups, reverse_code=remove_groups),
    ]
