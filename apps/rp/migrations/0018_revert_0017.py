from django.db import migrations
from django.contrib.auth.models import Group, User

def remove_groups(apps, schema_editor):
    groups = ['Jedi', 'Padawan', 'Droid']

    for i in groups:
        Group.objects.filter(name=i).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('rp', '0017_groups'),
    ]

    operations = [
        migrations.RunPython(remove_groups),
    ]
