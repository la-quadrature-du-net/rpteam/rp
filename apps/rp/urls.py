from django.conf.urls import url

from rp.views.articles import (ArticleListFlux, ArticleEdit,
                               ArticleDetailView, ArticleList)

urlpatterns = [
    url(
        r"^$",
        ArticleList.as_view(filter_lang='FR'),
        name="public-article-list"
    ),
    url(
        r"^international$",
        ArticleList.as_view(filter_lang='EN'),
        name="public-article-list-international"
    ),
    url(
        r"^by-tag/(?P<filter_tag>.*)",
        ArticleList.as_view(),
        name="public-article-list-tag"
    ),
    url(
        r"^article/list/(?P<filter_view>\w+)",
        ArticleListFlux.as_view(),
        name="article-list"
    ),
    url(
        r"^article/list",
        ArticleListFlux.as_view(),
        name="article-list"
    ),
    url(
        r"^article/edit/(?P<pk>\d+)",
        ArticleEdit.as_view(),
        name="article-edit"
    ),
    url(
        r"^article/view/(?P<pk>\d+)",
        ArticleDetailView.as_view(),
        name="article-view"
    ),
    url(
        r"^article/preview/(?P<pk>\d+)",
        ArticleDetailView.as_view(preview=True),
        name="article-preview"
    )
]
