from rest_framework import routers
from .views import ArticleViewSet, ArticleSearch, ArticleTag, ArticleStatus


router = routers.DefaultRouter()

router.register(r"articles", ArticleViewSet)
router.register(r"articles-by-status/(?P<filter_status>.+)", ArticleStatus)
router.register(r"articles-by-tag/(?P<filter_tag>.+)", ArticleTag)
router.register(r"articles-search/(?P<search_keywords>.+)", ArticleSearch)

urlpatterns = router.urls
