from django.db.models import Q

from rest_framework import viewsets, mixins
from djangorestframework_fsm.viewset_mixins import get_drf_fsm_mixin

from rp.models import Article
from rp.views.articles import ArticleFilterMixin

from .serializers import ArticleSerializer

ArticleMixin = get_drf_fsm_mixin(Article, fieldname='status')


class ArticleViewSet(ArticleMixin, ArticleFilterMixin, viewsets.ModelViewSet):
    """
    articles:
    This viewset describes all the method usable on the API
    using Article.

    The RequestBody is only needed for the create, update and partial_update
    functions.

    You can add query parameters to do some filtering, Those are :
        q="search terms" # to do a plaintext search into the title and extract
                         # of articles
        archive="true|[false]|both" # to filter on the archive flag or to
                                    # disable filtering on this flag.
        speak="true|false|[both]" # to filter on the speak flag, or to disable
                                  # filtering on this flag.
        quote="true|false|[both]" # to filter on the quote flag, or to disable
                                  # filtering on this flag.

    list:
    List all known articles in database.

    permissions ```None```

    create:
    Create an article from its URL and inserts it with the
    status _NEW_. If the article already exists, it is upvoted
    by one, and the tags are merged (in case new tags are given
    to this command).

    permissions ```None```

    read:
    Get a specific article by id.

    permissions ```None```

    update:
    Change the content of an article.

    permissions ```rp.can_edit```

    partial_update:
    Change only some fields of an article.

    permissions ```rp.can_edit```

    downvote:
    Decrement the score of an article by one. If the score goes below the
    ```ARTICLE_SCORE_THRESHOLD```, the article will stay in _DRAFT_ state.

    permissions ```rp.can_vote```

    upvote:
    Increment the score of an article by one. It it goes above the
    ```ARTICLE_SCORE_THRESHOLD```, its state will change to _DRAFT_.

    permissions ```rp.can_vote```

    recover:
    Force an Article status at _DRAFT_. It is used to recover _REJECTED_
    articles or ones with a score below ```ARTICLE_SCORE_THRESHOLD```.

    permissions ```rp.can_change_status```

    publish:
    Change the state of a _DRAFT_ article to _PUBLISHED_

    permissions ```rp.can_change_status```

    reject:
    Force an article status to _REJECTED_. It can only be brought back with
    the recover function.

    permissions ```rp.can_change_status```

    set_priority:
    Set the boolean priority of an article to True.

    permissions ```rp.can_change_priority```

    unset_priority:
    Set the boolean priority of an article to False.

    permissions ```rp.can_change_priority```

    set_flags:
    Set the boolean value of flags of an article. The flags are given as a
    dictionary of boolean such as :
        { 'quote': True, 'speak': False, 'archive': False }

    There is currently no other flags.

    permissions ```rp.can_edit```
    """
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer


class ArticleSearch(viewsets.ModelViewSet, mixins.ListModelMixin):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer

    def list(self, request, *args, **kwargs):
        self.queryset = self.queryset.filter(Q(title__icontains=kwargs['search_keywords'])
                                             | Q(extracts__icontains=kwargs['search_keywords']))
        return super().list(request, args, kwargs)


class ArticleTag(viewsets.ModelViewSet, mixins.ListModelMixin):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer

    def list(self, request, *args, **kwargs):
        self.queryset = self.queryset.filter(tags__name__in=[kwargs['filter_tag']]).distinct()
        return super().list(request, args, kwargs)


class ArticleStatus(viewsets.ModelViewSet, mixins.ListModelMixin):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer

    def list(self, request, *args, **kwargs):
        self.queryset = self.queryset.filter(status=kwargs['filter_status'].upper())
        return super().list(request, args, kwargs)
