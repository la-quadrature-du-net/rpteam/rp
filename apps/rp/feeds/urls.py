from django.conf.urls import url
from .feeds import ArticlesFeed, ArticlesTagsFeed, ArticlesSearchFeed

urlpatterns = [
    url(r'^$', ArticlesFeed(filter_lang='FR'), name='articles-feed'),
    url(r'^international$', ArticlesFeed(filter_lang='EN'), name='articles-feed-international'),
    url(r'^by-tag/(?P<filter_tag>.*)', ArticlesTagsFeed()),
    url(r'^search/(?P<search_keywords>.*)', ArticlesSearchFeed())
]
