from django.contrib.syndication.views import Feed
from django.db.models import Q

from rp.models import Article

class ArticlesFeed(Feed):
    title = "Revue de presse de la Quadrature"
    link = "/feeds/"
    description = "La revue de presse recense les articles de presse relatifs aux sujets de la Quadrature. Elle est compilée chaque jour par ses bénévoles, à partir de la presse francophone et internationale. Bonne lecture !"

    def __init__(self, **kwargs):
        self.filter_lang = kwargs['filter_lang']

    def items(self):
        return Article.objects.filter(lang=self.filter_lang) \
                              .order_by('-created_at')[:25]

    def item_title(self, item):
        return item.title

    def item_status(self, item):
        return item.status

    def item_description(self, item):
        return item.extracts

    def item_link(self, item):
        return item.url

    def item_pubdate(self, item):
        return item.published_at

    def item_updateddate(self, item):
        return item.updated_at

    def item_categories(self, item):
        return item.tags.all()

class ArticlesTagsFeed(ArticlesFeed):
    def __init__(self, **kwargs):
        pass

    def items(self):
        return Article.objects.filter(tags__name__in=[self.filter_tag]) \
                              .order_by('-created_at')[:25]

    def get_object(self, request, filter_tag):
        self.filter_tag = filter_tag

class ArticlesSearchFeed(ArticlesFeed):
    def __init__(self, **kwargs):
        pass

    def items(self):
        return Article.objects.filter(Q(title__icontains=self.search_keywords)
                                      | Q(extracts__icontains=self.search_keywords)) \
                                      .order_by('-created_at')[:25]

    def get_object(self, request, search_keywords):
        self.search_keywords = search_keywords
