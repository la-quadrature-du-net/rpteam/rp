import urlpy

URL_TRACKERS = [
    # UTM
    "utm_source", "utm_medium", "utm_campaign", "utm_name",

    # FB
    "fb", "fb_", "action_type_map", "action_ref_map", "action_object_map",
    "PHPSESSID",

    "xtor", "link_time", "__scoop",
    "_hs", "_hsenc", "_hsmi", "hsCtaTracking",
    "wkey", "wemail",
]

def cleanup_url(url_path, default_scheme="http"):
    """
    Sanitize a URL string and remove major tracker codes.

    Examples
    --------

    >>> cleanup_url("http://example.com/?utm_source=dlvr.it&utm_medium=twitter&utm_campaign=social")
    'http://example.com/'

    >>> cleanup_url("http://ü.com/??")
    'http://\xc3\xbc.com/'
    """
    u = urlpy.parse(url_path)
    u.defrag().deparam(URL_TRACKERS).canonical().unpunycode()

    if u.scheme == "":
        u.scheme = default_scheme

    return str(u.escape())

def tag_comma_splitter(tag_string):
    """
    According to https://django-taggit.readthedocs.io/en/latest/custom_tagging.html,
    this needs to be used by taggit using the settings TAGGIT_TAGS_FROM_STRING.

    Our tags will be case insensitive.
    """
    return [t.strip().lower() for t in tag_string.split(',') if t.strip()]

def tag_comma_joiner(tag_list):
    """
    According to https://django-taggit.readthedocs.io/en/latest/custom_tagging.html,
    this needs to be used by taggit using the settings TAGGIT_STRING_FROM_TAGS.

    Our tags will be case insensitive.
    """
    return ', '.join([t.name for t in tag_list])
