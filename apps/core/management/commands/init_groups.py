from django.core.management.base import BaseCommand
from django.contrib.auth.models import User, Group, Permission

groups = ["droid", "jedi", "padawan"]
permissions = {
    "droid": [],
    "jedi": [
        "can_change_status", "can_change_priority", "can_vote", "can_edit",
        "can_edit_users", "can_delete_users", "can_create_users",
    ],
    "padawan": ["can_vote", "add_article"]
}


class Command(BaseCommand):
    help = """
    Adds initial groups for the application (jedis, droids and padawans).
    Existing user are promoted as padawans and Superuser are promoted to jedi.
    """

    def handle(self, *args, **options):

        for g in groups:
            print("Creating group '{}'".format(g))
            new_group, created = Group.objects.get_or_create(name=g)
            for p in permissions[g]:
                new_group.permissions.add(Permission.objects.get(codename=p))

        users = User.objects.all()
        for user in users:
            if user.is_superuser:
                user.groups.add(Group.objects.get(name='jedi'))
            else:
                user.groups.add(Group.objects.get(name="padawan"))
