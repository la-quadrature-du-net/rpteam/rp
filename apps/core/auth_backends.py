from django.contrib.auth.backends import ModelBackend
from django.contrib.auth import get_user_model


class EmailOrUsernameModelBackend(ModelBackend):
    """
    We allow for email or username to be used as a  login.
    Case insensitive.
    """
    def authenticate(self, request, username=None, password=None):
        # If we call this function, it means we're not authenticated yet.
        # Since we're going through the classic Model backend first, it means
        # that the user does not exists.
        UserModel = get_user_model()

        try:
            user = UserModel.objects.get(email=username)

            if user.check_password(raw_password=password):
                return user
        except UserModel.DoesNotExist:
            # The User does not exist. Hashing the password anyway,
            # to limit Time attacks efficiency
            UserModel().set_password(raw_password=password)
            return None

    def get_user(self, user_id):
        # This is used to get the User objects
        UserModel = get_user_model()
        print("get_user: {}".format(user_id))

        try:
            return UserModel.objects.get(pk=user_id)
        except UserModel.DoesNotExist:
            return None

