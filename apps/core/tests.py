from django.test import TestCase
from django.test import Client, override_settings
from django_factory_boy import auth as auth_factories


@override_settings(AUTHENTICATION_BACKENDS=[
    # Default
    "django.contrib.auth.backends.ModelBackend",
    # Email or Username for login
    "core.auth_backends.EmailOrUsernameModelBackend"
    ])
class ApiTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = auth_factories.UserFactory(password="dummypassword")

    def test_authenticate(self):
        # username, wrong password
        login_status = self.client.login(
            username=self.user.username,
            password="notthepassword"
        )
        assert not login_status

        # username, correct password
        login_status = self.client.login(
            username=self.user.username,
            password="dummypassword"
        )
        assert login_status

        # email, wrong password
        login_status = self.client.login(
            username=self.user.email,
            password="notthepassowrd"
        )
        assert not login_status

        # email, correct password
        login_status = self.client.login(
            username=self.user.email,
            password="dummypassword"
        )
        assert login_status

        # username, wrong user
        login_status = self.client.login(
            username="{}-2".format(self.user.username),
            password="dummypassword"
        )
        assert not login_status

        # email, wrong user
        login_status = self.client.login(
            username="{}-2".format(self.user.email),
            password="dummypassword"
        )
        assert not login_status
