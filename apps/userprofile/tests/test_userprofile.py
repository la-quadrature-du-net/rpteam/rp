from django.contrib.auth.models import User, Permission
from django.template.defaultfilters import slugify
from django.test import Client, TestCase

from userprofile.apps import UserprofileConfig
from userprofile.models import Profile
from userprofile.factories import ProfileFactory


class ProfileTestCase(TestCase):
    def setUp(self):
        self.client = Client()

        p = Permission.objects.get(codename='can_edit_users')
        user = User.objects.create_user('test', 'test@example.org', 'test')
        user.user_permissions.add(p)
        self.user = user

    def test_init(self):
        assert UserprofileConfig.name == "userprofile"

    def test_profile(self):
        profile = ProfileFactory()
        assert type(profile.user) == User
        assert type(profile) == Profile
        assert profile.slug == slugify(profile.user.username)
        assert str(profile) == profile.user.username

        profile.delete()

    def test_list(self):
        self.client.force_login(self.user)
        r = self.client.get('/users/list/')
        assert r.status_code == 200
        assert self.user.email in str(r.content)
