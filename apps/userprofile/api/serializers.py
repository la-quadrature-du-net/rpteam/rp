from userprofile.models import Profile
from rest_framework import serializers


class ProfileSerializer(serializers.ModelSerializer):
    """A simple profile serializers"""
    avatar = serializers.SerializerMethodField()

    class Meta:
        model = Profile
        fields = "__all__"

    def get_avatar(self, instance):
        if not instance.picture:
            return None
        return instance.picture.url
