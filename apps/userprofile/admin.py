from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User, Permission
from django.utils.translation import ugettext_lazy as _

from userprofile.models import Profile


@admin.register(Permission)
class PermissionAdmin(admin.ModelAdmin):
    list_display = ("name", "codename", "content_type")
    list_filter = ("content_type",)


class UserProfileInline(admin.StackedInline):
    model = Profile
    list_display = ("user", "full_name")

    extra = 1
    max_num = 1


class UserProfileAdmin(UserAdmin):
    inlines = [UserProfileInline]

    list_display = ("username", "email", "first_name", "last_name",
                    "is_staff", "get_groups")

    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (_("Personal info"), {"fields": ("first_name", "last_name", "email")}),
        (_("Permissions"), {
            "fields": ("is_active", "is_staff", "is_superuser", "groups")}),
    )

    def get_groups(self, obj):
        return ", ".join(sorted([g.name for g in obj.groups.all()]))
    get_groups.short_description = _("Groups")


admin.site.unregister(User)
admin.site.register(User, UserProfileAdmin)
