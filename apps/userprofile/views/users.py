from django.urls import reverse_lazy

from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from django.views.generic.list import ListView
from django.views.generic.edit import DeleteView, UpdateView


class UserListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = User
    paginate_by = 20
    template_name = 'user/user_list.html'
    permission_required = 'userprofile.can_edit_users'

    def get_queryset(self):
        qs = super().get_queryset()

        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context


class UserEditView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = User
    permission_required = 'userprofile.can_edit_users'
    template_name = 'user/user_update_form.html'
    context_object_name = 'user_edit'

    fields = ['groups']
    success_url = reverse_lazy("users:list")


class UserDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    model = User
    permission_required = 'userprofile.can_delete_users'
    template_name = 'user/user_delete_confirm.html'
    context_object_name = 'user_delete'

    success_url = reverse_lazy("users:list")
