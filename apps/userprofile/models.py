from imagekit.models import ImageSpecField
from pilkit.processors import ResizeToFill

from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext as _


class Profile(models.Model):
    """
    Extends `django.contrib.auth.models.User` with additional fields
    """

    #: OneToOneField to ``django.contrib.auth.models.User``
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        related_name="profile"
    )

    #: User description
    description = models.CharField(max_length=200, null=True, blank=True)

    #: User avatar
    picture = models.ImageField(
        upload_to="uploads/users/%Y/%m/%d",
        null=True,
        blank=True
    )

    #: 50x50 thumbnail of user picture
    picture_t = ImageSpecField(
        source="picture",
        processors=[ResizeToFill(50, 50)],
        format="jpeg",
    )

    class Meta:
        verbose_name = _("User")
        app_label = "userprofile"
        permissions = (
            ("can_edit_users", "Can edit users",),
            ("can_delete_users", "Can delete users",),
            ("can_create_users", "Can create users",),
        )

    def __str__(self):
        """Returns user username"""
        return self.user.username

    def delete(self, *args, **kwargs):
        """Deletes ``django.contrib.auth.models.User`` and
        this profile"""
        self.user.delete()
        return super().delete(*args, **kwargs)

    @property
    def slug(self):
        """
        Returns slug version of user.username
        """
        return slugify(self.user.username)
