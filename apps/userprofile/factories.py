import factory
from django_factory_boy import auth as auth_factories

from .models import Profile


class ProfileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Profile

    user = factory.SubFactory(auth_factories.UserFactory)
    description = factory.Faker("text")
    picture = factory.django.ImageField()
