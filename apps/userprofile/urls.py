from django.conf.urls import url

from userprofile.views.users import UserDeleteView, UserEditView, UserListView

urlpatterns = [
    url(
        r"^list",
        UserListView.as_view(),
        name="list"
    ),

    url(
        r"^edit/(?P<pk>\d+)",
        UserEditView.as_view(),
        name="edit"
    ),

    url(
        r"^delete/(?P<pk>\d+)",
        UserDeleteView.as_view(),
        name="delete"
    )
]
