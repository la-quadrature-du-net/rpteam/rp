$(function () {
  function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
	var cookie = jQuery.trim(cookies[i]);
	// Does this cookie string begin with the name we want?
	if (cookie.substring(0, name.length + 1) === (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
	}
      }
    }
    return cookieValue;
  }
  var csrftoken = getCookie('csrftoken');

  function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  }
  $.ajaxSetup({
    beforeSend: function(xhr, settings) {
      if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
	xhr.setRequestHeader("X-CSRFToken", csrftoken);
      }
    }
  });

  window.call_upvote = function(id) {
    $.post("/api/articles/" + id + "/upvote/", function response(data) {
      $("#score_" + id).text(data.score);
    });
  }

  window.call_downvote = function(id) {
    $.post("/api/articles/" + id + "/downvote/", function response(data) {
      $("#score_" + id).text(data.score);
    });
  }

  window.clean_row = function(id) {
    $("#row_" + id).hide();
    $("#row_empty_" + id).hide();
    $("#row_tags_" + id).hide();
  }

  window.call_recover = function(id) {
    $.post("/api/articles/" + id + "/recover/", function response(data) {
      clean_row(id);
    });
  }

  window.call_reject = function(id) {
    $.post("/api/articles/" + id + "/reject/", function response(data) {
      clean_row(id);
    });
  }

  window.call_publish = function(id) {
    $.post("/api/articles/" + id + "/publish/", function response(data) {
      clean_row(id);
    });
  }

  window.call_priority = function(id, flag) {
    if(flag) {
      var url = "/api/articles/" + id + "/set_priority/";
    } else {
      var url = "/api/articles/" + id + "/unset_priority/";
    }

    $.post(url, function response(data) {
      $("#priority_" + id).toggleClass("fa-star").toggleClass("fa-star-o");
    });
  }
});
