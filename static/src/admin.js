// Vendor styles and scripts
global.$ = global.jQuery = require('jquery');
global.Tether = require('tether');
require('bootstrap/dist/js/bootstrap.js');

require('font-awesome/css/font-awesome.css');
require('bootstrap/dist/css/bootstrap.css');

require('select2');
require('select2/dist/css/select2.css');
require('select2-bootstrap-theme/dist/select2-bootstrap.css');

// Markdown editor
global.SimpleMDE = require('simplemde');
require('simplemde/dist/simplemde.min.css')

// Local styles
import styles from './admin.css';

// RP js
require('./rp.js')
