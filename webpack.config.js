const webpack = require('webpack');
const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
  context: path.resolve(__dirname, 'static/src'),
  entry: {
    app: './admin.js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      }
    ]
  },
  output: {
    path: path.resolve(__dirname, 'static/dist'),
    filename: "./admin.min.js"
  },
  plugins: [
    new MiniCssExtractPlugin({'filename': '[name].bundle.css'})
  ],
  optimization: {
    minimizer: [new UglifyJsPlugin()],
  },
};
