# LQDN RP

This project uses Django 2.2 and Python 3.5 or 3.6.

## Requirements

To clone the repository, [git-lfs](https://git-lfs.github.com/) must be installed.

To run the project, start a virtual environment:

    $ virtualenv -p python3 env
    $ source ./env/bin/activate

and install all required Python dependencies for development and build:

    $ pip install -r requirements/base.txt
    $ pip install -r requirements/dev.txt

To compile the CSS stylesheets and JS scripts, [install npm](https://www.npmjs.com/get-npm), then run
the following commands to run yarn (to fetch and install dependencies) and
webpack (to compile local assets)

    $ npm install -g yarn webpack
    $ yarn install
    $ yarn run webpack

In case you want to use the fixtures, you will need to install (git-lfs)[https://git-lfs.github.com/].

## Settings

Create a file in `project/settings/env.py` which contains django
settings for **DEBUG**, **SECRET_KEY** and **SITE_ID**.

For testing you can use:

    DEBUG = True
    SECRET_KEY = "a random string"
    SITE_ID = 1

In production you can use env variables with

```python
os.environ["DJANGO_DEBUG"]
os.environ["DJANGO_SECRET_KEY"]
os.environ["DJANGO_SITE_ID"]
```

You'll also need to define 

```python
os.environ["DJANGO_ALLOWED_HOSTS"]
```

containing the FQDN of the RP before being able to run in production.

**Warning** :If DEBUG is defined as True, ALLOWED_HOSTS is set to allow connections
from any hosts.

## Database

You can run database migrations with :

```sh
    $ python manage.py migrate
```

Groups are initialized with the following command:
```sh
    $ python manage.py init_groups
```

You can seed the database with a provided fixtures
located in apps/rp/fixtures/all_articles.json:

```sh
    $ python manage.py loaddata all_articles.json
```

## Dev server

To launch the dev server :

```sh
    $ python manage.py runserver
```

## Installation in production

You can use any uWSGI server configuration to run it as a normal Django app. See [this tutorial for more information.](https://www.digitalocean.com/community/tutorials/how-to-serve-django-applications-with-uwsgi-and-nginx-on-ubuntu-16-04).

## Tests

To launch tests install requirements/tests.txt

```sh
    $ pip install -r requirements/tests.txt
    $ pytest
```

Coverage with

```sh
    $ pytest --cov=.
```

## Upgrading requirements

Python requirements are pinned in `requirements/*.txt`. To upgrade those pinned versions, run:

    pip-compile --upgrade requirements/base.in
    pip-compile --upgrade requirements/dev.in
    pip-compile --upgrade requirements/tests.in
