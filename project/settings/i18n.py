"""
Internationalization and localization settings
See https://docs.djangoproject.com/en/1.10/topics/i18n/ for refs
"""

import os
from .base import BASE_DIR

# TODO : use en in backend and user lc in frontend
LANGUAGE_CODE = "en-US"

# TODO : use UTC in backend and use user tz in frontend
TIME_ZONE = "UTC"

USE_I18N = True
USE_L10N = True
USE_TZ = True

# Adds languages here
LANGUAGES = (
    ("en", "English"),
    ("fr", "French"),
)

LOCALE_PATHS = [os.path.join(BASE_DIR, "locale")]
