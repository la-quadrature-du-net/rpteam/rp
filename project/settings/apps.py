"""
Django installed apps
"""

from .env import DEBUG

DJANGO_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.contenttypes",
    "django.contrib.sites"
]

CONTRIB_APPS = [
    "django_extensions",  # http://django-extensions.readthedocs.io/
    "rest_framework",  # http://www.django-rest-framework.org/
    "rest_framework.authtoken",

    "taggit",
    "taggit_serializer",
    "crispy_forms",
    "django_markdown2",
    "sorl.thumbnail",

    "allauth",
    "allauth.account",
    "allauth.socialaccount",

    "django_fsm"
]

if DEBUG:
    CONTRIB_APPS.append(
        'debug_toolbar',  # https://django-debug-toolbar.readthedocs.io/
    )


PROJECT_APPS = [
    "userprofile",
    "core",
    "rp"
]

INSTALLED_APPS = DJANGO_APPS + CONTRIB_APPS + PROJECT_APPS

# Use username for upvote instead of user object
UND_USE_USERNAME = True

# Let's use a bootstrap4 template for crispy
CRISPY_TEMPLATE_PACK = "bootstrap4"

# We're using some dumber string parsing functions in taggit
TAGGIT_TAGS_FROM_STRING = 'rp.utils.tag_comma_splitter'
TAGGIT_STRING_FROM_TAGS = 'rp.utils.tag_comma_joiner'
