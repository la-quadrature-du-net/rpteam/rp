"""
django-rest-framework and api related settings
"""

REST_FRAMEWORK = {
    "DEFAULT_PAGINATION_CLASS": ("rest_framework.pagination."
                                 "PageNumberPagination"),
    "PAGE_SIZE": 20,

    "DEFAULT_PERMISSION_CLASSES": (
        "rest_framework.permissions.IsAuthenticatedOrReadOnly",
    ),
    # For issue #61
    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema',

    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    )
}
