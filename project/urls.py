"""
rp_new URL Configuration
"""

from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic.base import RedirectView

# Django rest framework router
from core.routers import DefaultRouter
from rp.api.urls import router as rp_router

# Django rest framework documentation
from rest_framework.documentation import include_docs_urls


router = DefaultRouter()
router.extend(rp_router)

urlpatterns = [
    url(r"^admin/", admin.site.urls),
    url(r'^$', RedirectView.as_view(url='/rp/', permanent=False), name='index'),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r"^api/", include((router.urls, "api"))),
    url(r"^feeds/", include(("rp.feeds.urls", "feeds"))),
    url(r"^rp/", include(("rp.urls", "rp"))),
    url(r'^accounts/', include('allauth.urls')),
    url(r"^users/", include(('userprofile.urls', "users"))),
    url(r"^docs/api/", include_docs_urls(title="API de la revue de presse")),
]

if settings.DEBUG:
    # Serve static files in DEBUG mode
    urlpatterns += static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT
    )

    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
